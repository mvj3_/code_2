# 10.6 osx 安装php
1. 配置apache虚拟主机信息, 往 /etc/apache2/extra/httpd-vhosts.conf 写入项目目录配置
<VirtualHost *:80>
  ServerAdmin your@email.com
  DocumentRoot "/Users/mvj3/eoemobile/code/eoe"
  ServerName edu.eoe.lo
  ErrorLog "/private/var/log/apache2/dummy-host2.example.com-error_log"
  CustomLog "/private/var/log/apache2/dummy-host2.example.com-access_log" common
</VirtualHost>
2. 配置apache, 编辑 /etc/apache2/httpd.conf
去掉 LoadModule php5_module        libexec/apache2/libphp5.so 的注释
由于我基本没用过apache，我把该配置里的其他默认项目目录 都更改到 /Users/mvj3/eoemobile/code/eoe
3. 在/Users/mvj3/eoemobile/code/eoe 添加phpinfo.php文件，并写入如下内容
 <?php echo phpinfo();
4. 重启apache
sudo apachectl restart
5. 访问edu.eoe.lo/phpinfo.php即可看到你本机的php配置信息

# 通过socket连接mysql
  ThinkPHP Can't connect to local MySQL server through socket '/tmp/mysql5.sock:3306'
  http://ggmmchou.blog.163.com/blog/static/59333149201112192652560/
  // 如果使用 localhost:/tmp/mysql5.sock 就指定DB_PORT=''
  'DB_HOST'=>'localhost:/tmp/mysql5.sock'